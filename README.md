# tech-notes

## Useful Terraform ecosystem addons 
- **TF docs** - creates terraform documentation based on modules / variables etc - https://terraform-docs.io/user-guide/introduction/
- **infracost** - estimates how much your monthly cost will be based on the terraform config - https://www.infracost.io/

## Useful VS Code extensions
* Python
* Pylance
* GitLens
* Python Test Explorer
* Python Docstring generator
* VS IntelliCode
* Django
* Flask
* Pytest
* Terraform
* Docker